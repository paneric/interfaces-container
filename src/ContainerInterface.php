<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Container;

use Psr\Container\ContainerInterface as Psr11ContainerInterface;

interface ContainerInterface extends Psr11ContainerInterface
{
    public function set(string $name, $value);
}
